/*************************************************************************

   AUTHOR  :  

   SOURCE  :  hw9.cpp

   DATE    :  

   COMPILER: Visual C++ .NET

   ACTION  : The program tests routines designed to perform various
             operations on singly linked lists. The lists have a
             dummy head node that holds the "Happy Face" character. 
             The tail of the lists points to NULL.

*************************************************************************/
/****************************   ReadList    *****************************

DESCRIPTION   Builds a singly linked list with a dummy head node. The
characters in the list are read in from an external file
in the same order in which they are found in file.

Input to list terminates when the End of File is encountered

PARAMETERS

IN, List    A pointer to a singly linked list with a dummy head node.
It is imperative that List be initialized before calling
this routine.

IN, FileName  A pointer to a string that has the name of the file to
open, if error in opening then return a 1;

RETURNS      1 if file opening error, 0 if read from file successful

NOTE          Before building the new list, ZapList is called. This
ensures that a memory leak does not develop.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
/*This function header in a9shell.cpp is different than on the assignment pdf*/
int ReadList(NodePtr List, char FileName[])
{
    ifstream FileIn(FileName); //declare file variable to read from
    char Ch;
    NodePtr CurrentNode;
    NodePtr NewNode;

    CurrentNode = List;
    NewNode = NULL;

    if (!FileIn)
        return 1;

    ZapList(List);  //make sure list is empty

    FileIn.get(Ch); //Prime read

    do
    {
        NewNode = new(nothrow)Node; //Attempt allocation

        if (NewNode == NULL) //Allocation error, return 2
            return 2;        //as shown on assignment pdf

        NewNode->Ch = Ch;            //Add NewChar to list
        NewNode->Link = NULL;        //Initialize Link to NULL
        CurrentNode->Link = NewNode; //Links Current to New

        FileIn.get(Ch);         //Get next char
        CurrentNode = NewNode;  //Swap New into Current for next iteration.
    } while (FileIn.good());

    FileIn.close();

    return 0;

}

/**************************  SortList ************************************

Description  Arranges the singly linked list pointed to by List in
natural order.  It is assumed that the list has a dummy
head node.

The algorithm used is a linked variation of the selection
sort and works like this:
Start with EndSorted aimed at first node of list

repeat
Find smallest char between EndSorted and end of list
Swap smallest element with char in EndSorted
Change EndSorted to next node
until we get to end of list

None of the pointers in linked list are changed

Parameters
IN, List  A pointer to a singly linked list with a dummy head node
-----------------------------------------------------------------------*/
void SortList(NodePtr List)
{
    NodePtr SmallNode;        //points to smallest char
    NodePtr SearchNode;       //used to search each node in list
    NodePtr EndSorted;        //points to list to sort
    char TempCh;

    EndSorted = List->Link;
    SmallNode = NULL;

    while (EndSorted != NULL)
    {
        SearchNode = EndSorted;
		SmallNode = EndSorted;    //Initialize to first possible smallest
        while (SearchNode != NULL)
        {
            if (SearchNode->Ch < SmallNode->Ch) //If the data is smaller
                SmallNode = SearchNode;         //New smallest node

            SearchNode = SearchNode->Link; //Iterate next in list
        }

        TempCh = SmallNode->Ch;         
        SmallNode->Ch = EndSorted->Ch;  //Swap characters in list
        EndSorted->Ch = TempCh;

        EndSorted = EndSorted->Link; //Iterate next unsorted
    }
}